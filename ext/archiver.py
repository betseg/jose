import os
import io
import time
import json
import zipfile
import tempfile
import logging
from pathlib import Path
from typing import Tuple

import discord
from discord.ext import commands

from .common import Cog, SayException
from .stars_archiver import write_json

log = logging.getLogger(__name__)


KILOBYTE = 1024
MEGABYTE = 1024 * KILOBYTE


def mk_zip(guild_id: int) -> Tuple[Path, zipfile.ZipFile]:
    """Make a zip file for a guild archive."""
    folder = Path("archives")
    folder.mkdir(exist_ok=True)

    zip_path = folder / f"archive-{guild_id}.zip"
    zip_file = zipfile.ZipFile(zip_path, mode="w", compression=zipfile.ZIP_DEFLATED)
    return zip_path, zip_file


async def write_str_thread(ctx, zip_file, path, string: str):
    """Write a string to a zip file in a thread."""
    await ctx.bot.loop.run_in_executor(None, zip_file.writestr, path, string)


async def write_ndjson(ctx, zip_file, path, lst: list):
    """Serialize a list of json data as NDJSON and write it to the zip file in a thread."""
    res = "\n".join(map(json.dumps, lst))
    await ctx.bot.loop.run_in_executor(None, zip_file.writestr, path, res)


def color_(val):
    if val is None:
        return None

    if isinstance(val, int):
        return val

    return val.value


def _jsonify_embed(embed: discord.Embed):
    # TODO: fuller embed data, this is just too simple.
    # however, querying stuff over EmbedProxy might be very laggy
    return {
        "title": embed.title,
        "type": embed.type,
        "description": embed.description,
        "url": embed.url,
        "color": color_(embed.color),
    }


def _jsonify_attach(attach: discord.Attachment):
    return {
        "id": attach.id,
        "size": attach.size,
        "filename": attach.filename,
        "url": attach.url,
        "proxy_url": attach.proxy_url,
    }


def jsonify_embeds(embeds):
    """JSONify embeds."""
    return list(map(_jsonify_embed, embeds))


def jsonify_attachments(attachs):
    """JSONify attachments."""
    return list(map(_jsonify_attach, attachs))


def to_dict(obj) -> dict:
    """Convert instance to dictionary."""
    return dict(
        (name, getattr(obj, name)) for name in dir(obj) if not name.startswith("__")
    )


async def _ar_guild_main(guild, zip_file):
    log.debug("guild main gid=%d", guild.id)

    gjson = {
        "id": guild.id,
        "name": guild.name,
        "emojis": [
            {
                "id": emoji.id,
                "name": emoji.name,
                "animated": emoji.animated,
                "url": str(emoji.url),
            }
            for emoji in guild.emojis
        ],
        "icon": guild.icon,
        "icon_url": str(guild.icon_url),
        "owner_id": guild.owner_id,
        "member_count": guild.member_count,
        "channels": [
            {
                "id": chan.id,
                "type_name": type(chan).__name__,
                "parent_id": chan.category.id if chan.category else None,
                "name": chan.name,
            }
            for chan in guild.channels
        ],
    }

    await write_json(zip_file, "guild.json", gjson)


async def _ar_guild_members(ctx, zip_file):
    log.debug("members gid=%d", ctx.guild.id)

    res = []
    guild = ctx.guild

    for member in guild.members:
        res.append(
            {
                "id": member.id,
                "joined_at": timestamp_(member.joined_at),
                "nick": member.nick,
                "display_name": member.display_name,
            }
        )

    await write_ndjson(ctx, zip_file, "members.json", res)


async def _archive_guild(ctx, zip_file):
    guild = ctx.guild
    await _ar_guild_main(guild, zip_file)
    await _ar_guild_members(ctx, zip_file)


def timestamp_(dtime):
    """Convert a datetime instance into an ISO formatted timestamp"""
    return dtime.isoformat() if dtime is not None else None


class Archiver(Cog):
    """Guild message archive."""

    def __init__(self, bot):
        super().__init__(bot)
        self._jobs = {}

    async def _archive_single_loop(self, ctx, tmpfd, chan):
        count = 0

        job = self._jobs[ctx.guild.id]
        job["channel_id"] = chan.id

        async for message in chan.history(limit=None, reverse=False):
            if count % 25000 == 0 and count > 0:
                await ctx.send(
                    f"still working on {chan.mention}, " f"current count: {count}"
                )

            valid_json = {
                "id": message.id,
                "type": message.type.value,
                "author_id": message.author.id,
                "content": message.content,
                "mention_everyone": message.mention_everyone,
                "mentions": [m.id for m in message.mentions],
                "channel_mentions": [c.id for c in message.channel_mentions],
                "role_mentions": [r.id for r in message.role_mentions],
                "webhook_id": message.webhook_id,
                "pinned": message.pinned,
                "edited_at": timestamp_(message.edited_at),
                "embeds": jsonify_embeds(message.embeds),
                "attachments": jsonify_attachments(message.attachments),
            }

            json_str = json.dumps(valid_json) + "\n"
            tmpfd.write(json_str.encode())

            count += 1
            job["stats"]["messages_done"] += 1

        if count > 25000:
            await ctx.send(f"finished {chan.mention}, at {count} messages")

        return count

    async def _archive_single(self, ctx, zip_file, chan):
        tmpfdn, tmppath = tempfile.mkstemp(suffix=".ndjson")
        tmpfd = os.fdopen(tmpfdn, "wb")

        log.info("archiving channel to temporary %r", tmppath)

        count = None

        try:
            count = await self._archive_single_loop(ctx, tmpfd, chan)

            tmpfd.close()
            zip_file.write(tmppath, arcname=f"messages/{chan.id}.ndjson")
        finally:
            os.unlink(tmppath)

        return count

    async def _archive_main(self, zip_file, ctx):
        await _archive_guild(ctx, zip_file)

        for chan in ctx.guild.text_channels:
            log.info("working channel gid=%d cid=%d", ctx.guild.id, chan.id)

            try:
                count = await self._archive_single(ctx, zip_file, chan)
            except discord.errors.Forbidden:
                await ctx.send(f"failed to archive {chan.mention}, no perms")
                continue

            log.info(
                "channel worked gid=%d cid=%d count=%d", ctx.guild.id, chan.id, count
            )

            stats = self._jobs[ctx.guild.id]["stats"]
            stats["channels_done"] += 1

    async def _archive_worker(self, ctx):
        """Main archive worker"""
        zip_path, zip_file = mk_zip(ctx.guild.id)

        log.info("starting archiver for %d", ctx.guild.id)
        await self._archive_main(zip_file, ctx)

        zip_file.close()

        attach = discord.File(zip_path, filename=f"archival-{ctx.guild.id}.zip")

        zip_size = os.path.getsize(zip_path)
        zip_size_mb = round(zip_size / 1024 / 1024)

        if zip_size <= 8 * MEGABYTE:
            return await ctx.send("finished!\n", file=attach)

        await ctx.send(
            "oops. your archive is "
            f"{zip_size_mb}mb in size.\n"
            "Discord does not allow users without nitro to"
            "upload more than 8MB.\n"
            "The archive is saved manually on the server, ask"
            f"the bot owner ({ctx.prefix}about) for a manual transfer"
        )

    async def _archive_wrapper(self, ctx):
        try:
            await self._archive_worker(ctx)
        except Exception as err:
            log.exception("failure while running archiver worker")
            await ctx.send(f"error while archiving. {err!r}")
        finally:
            try:
                self._jobs.pop(ctx.guild.id)
            except KeyError:
                log.warning("inconsistent job state")

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_guild=True)
    async def archive(self, ctx):
        """Start an archival job for this guild.

        The archiver will generate a ZIP file containing
        all messages in all channels for the guild.

        It is a long job.
        """
        if ctx.guild.id in self._jobs:
            raise SayException("archival already underway")

        self._jobs[ctx.guild.id] = {
            "task": self.loop.create_task(self._archive_wrapper(ctx)),
            "channel_id": None,
            "stats": {"channels_done": 0, "messages_done": 0},
        }

        await ctx.ok()

    @commands.command()
    @commands.guild_only()
    async def arinfo(self, ctx):
        """Get archival info"""
        try:
            job = self._jobs[ctx.guild.id]
        except KeyError:
            raise SayException("Archival job not found.")

        stats = job["stats"]
        task = job["task"]

        await ctx.send(
            f"running? {not task.cancelled()}\n"
            f'current channel: <#{job["channel_id"]}>\n'
            f'channels done: {stats["channels_done"]}\n'
            f'messages done: {stats["messages_done"]}'
        )


async def setup(bot):
    await bot.add_cog(Archiver(bot))
