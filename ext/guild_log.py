import logging
import asyncio

import logging
import discord

from .common import Cog

log = logging.getLogger(__name__)


class GuildLog(Cog):
    def __init__(self, bot):
        super().__init__(bot)
        self.webhook = discord.Webhook.from_url(
            self.bot.config["extra_logs"]["guild_joinlogs"], session=self.bot.session
        )

    def guild_embed(self, em, guild):
        em.set_thumbnail(url=str(guild.icon))

        em.add_field(name="guild id", value=guild.id)

        em.add_field(name="guild name", value=guild.name)
        em.add_field(name="guild owner", value=guild.owner)
        em.add_field(name="guild region", value=str(guild.region))
        em.add_field(name="guild member count", value=guild.member_count)
        em.add_field(name="guild large?", value=guild.large)
        em.add_field(name="guild <- shard id", value=guild.shard_id)

    @Cog.listener()
    async def on_guild_join(self, guild):
        if not self.bot.is_ready():
            return

        em = discord.Embed(title="Guild join", color=discord.Color.green())
        self.guild_embed(em, guild)
        await self.webhook.send(embed=em)

    @Cog.listener()
    async def on_guild_remove(self, guild):
        if not self.bot.is_ready():
            return

        em = discord.Embed(title="Guild remove", color=discord.Color.red())
        self.guild_embed(em, guild)
        await self.webhook.send(embed=em)

    @Cog.listener()
    async def on_guild_unavailable(self, guild):
        if not self.bot.is_ready():
            return

        # we log instead of embed because the logging monkeypatcher
        # handles ratelimiting well which is good when the thundering herd
        # of guild available/unavailable come up to the bot
        log.warning("Guild unavailable: %d %r", guild.id, guild.name)

        # em = discord.Embed(title="Guild unavailable", color=discord.Color(0xFCD15C))
        # self.guild_embed(em, guild)
        # await self.webhook.execute(".", embed=em)

    @Cog.listener()
    async def on_guild_available(self, guild):
        if not self.bot.is_ready():
            return

        log.warning("Guild available: %d %r", guild.id, guild.name)

        # em = discord.Embed(title="Guild available", color=discord.Color(0x00F00F))
        # self.guild_embed(em, guild)
        # await self.webhook.execute(".", embed=em)


async def setup(bot):
    await bot.add_cog(GuildLog(bot))
