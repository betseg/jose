import pprint
import asyncio
import json
import collections
import time
import logging

import asyncpg
import discord
from discord.ext import commands

from .common import Cog

log = logging.getLogger(__name__)


def _is_moderator(ctx):
    if not ctx.guild:
        return False

    is_owner_bool = ctx.author.id == ctx.bot.owner_id
    perms = ctx.channel.permissions_for(ctx.author)
    return is_owner_bool or perms.manage_guild


def is_moderator():
    return commands.check(_is_moderator)


async def _set_json(con):
    """Set JSON and JSONB codecs for an
    asyncpg connection."""
    await con.set_type_codec(
        "json", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
    )

    await con.set_type_codec(
        "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
    )


class Config(Cog):
    """Guild-specific configuration commands."""

    def __init__(self, bot):
        super().__init__(bot)

        # querying the db every time is not worth it
        self.config_cache = collections.defaultdict(dict)

        # used to check if cache has all defined objects in it
        self.default_keys = None

        # asyncpg connection pool
        self.db = None
        self.db_available = asyncio.Event()

        # reconnect using different methods
        # depending on the loop state
        if self.loop.is_running():
            self.loop.create_task(self.pg_init())
        else:
            self.loop.run_until_complete(self.pg_init())

    async def pg_init(self):
        self.db = await asyncpg.create_pool(**self.bot.config["postgres"])
        self.bot.db = self.db
        self.db_available.set()

    async def block_one(self, user_id: int, field="user_id", reason=None):
        """Block one thing from using jose."""
        reason = await self.db.fetchval(
            f"""
        SELECT reason
        FROM blocks
        WHERE {field} = $1
        """,
            user_id,
        )

        if reason is not None:
            return False

        user_id = user_id if field == "user_id" else None
        guild_id = user_id if field == "guild_id" else None

        res = await self.db.execute(
            """
        INSERT INTO blocks (user_id, guild_id, reason)
        VALUES ($1, $2, $3)
        """,
            user_id,
            guild_id,
            reason,
        )

        self.bot.block_cache[user_id] = True

        _, _2, resval = res.split()
        resval = int(resval)
        return resval > 0

    async def unblock_one(self, user_id, field="user_id"):
        """Unblock one thing from jose."""
        res = await self.db.execute(
            f"""
        DELETE FROM blocks
        WHERE {field} = $1
        """,
            user_id,
        )
        self.bot.block_cache[user_id] = False

        # maintain compatibility with mongodb code
        _, deleted = res.split()
        deleted = int(deleted)
        return deleted > 0

    async def ensure_cfg(self, guild, query=False) -> dict:
        """Get a configuration object for a guild.
        If `query` is `False`, it checks for a configuration object in cache

        Parameters
        ----------
        guild: discord.Guild
            The guild to find a configuration object to.
        query: bool
            If this will check cache first or just query the underlying db
            for configuration data.
        """
        cached = self.config_cache[guild.id]

        # only return cached when we don't want to
        # query at all
        if not query and cached:
            return cached

        async with self.db.acquire() as conn:
            await _set_json(conn)

            cfg = await conn.fetchrow(
                """
            SELECT *
            FROM guild_configs
            WHERE guild_id = $1
            """,
                guild.id,
            )

        if not cfg:
            # Create a config for the guild, since it doesn't
            # exist.
            try:
                await self.db.execute(
                    """
                INSERT INTO guild_configs (guild_id, prefix)
                VALUES ($1, $2)
                """,
                    guild.id,
                    self.bot.config["discord"]["global_prefix"],
                )
            except asyncpg.UniqueViolationError:
                pass

            # Recall ensure_cfg for caching
            return await self.ensure_cfg(guild)

        # We have a proper config object, cache it.
        cfg = dict(cfg)
        self.config_cache[guild.id] = cfg
        return cfg

    async def cfg_get(
        self, guild: discord.Guild, key: str, default: "any" = None
    ) -> "any":
        """Get a configuration key for a guild."""
        if key in self.config_cache[guild.id]:
            return self.config_cache[guild.id][key]

        cfg = await self.ensure_cfg(guild)

        try:
            return cfg[key]
        except KeyError:
            cfg[key] = default
            return default

    async def cfg_set(self, guild, key: str, value: "any") -> bool:
        """Set a configuration key."""
        await self.ensure_cfg(guild)

        async with self.db.acquire() as conn:
            await _set_json(conn)

            res = await conn.execute(
                f"""
            UPDATE guild_configs
            SET {key} = $1
            WHERE guild_id = $2
            """,
                value,
                guild.id,
            )

        log.debug("[cfg:set] %s[gid=%d] k=%r <- v=%r", guild, guild.id, key, value)

        self.config_cache[guild.id][key] = value

        _, modified = res.split()
        modified = int(modified)
        return modified > 0

    @commands.command(name="cfg_get")
    @commands.guild_only()
    async def _config_get(self, ctx, key: str):
        """Get a configuration key"""
        res = await self.cfg_get(ctx.guild, key)
        await ctx.send(f"{res!r}")

    @commands.command(name="cfgall", hidden=True)
    @commands.guild_only()
    async def cfgall(self, ctx):
        """Get the configuration object for a guild."""
        tstart = time.monotonic()
        cfg = await self.ensure_cfg(ctx.guild)
        tend = time.monotonic()

        delta = round((tend - tstart) * 1000, 2)
        await ctx.send(f"```py\n" f"{pprint.pformat(cfg)}\n" f"Took {delta}ms.\n" "```")

    @commands.command(aliases=["speakchan"])
    @commands.guild_only()
    @is_moderator()
    async def speakchannel(self, ctx, channel: discord.TextChannel):
        """Set the channel José will gather messages to feed
        to his markov generator.

        By default, it will choose the same channel the command is invoked from.

        Please use a channel that has enough messages so that José can generate
        meaningful content out of it.

        Only a single channel may be selected at a time since fetching messages
        takes a long time.
        """
        old_id = await self.cfg_get(ctx.guild, "speak_channel")
        success = await self.cfg_set(ctx.guild, "speak_channel", channel.id)

        # invalidate texter
        speak = self.bot.get_cog("Speak")
        old = self.bot.get_channel(old_id)
        if speak and old:
            try:
                log.debug(
                    f"invalidating texter on {ctx.guild} "
                    f"{ctx.guild.id}, {old} {old.id} "
                    f"=> {channel} {channel.id}"
                )

                speak.text_generators.pop(ctx.guild.id)
            except KeyError:
                pass

        await ctx.success(success)

    @commands.command()
    @commands.guild_only()
    @is_moderator()
    async def jsprob(self, ctx, prob: float):
        """Set the probability per message that José will autoreply to it."""
        if prob < 0 or prob > 5:
            await ctx.send("`prob` is out of the range `[0-5]`")
            return

        success = await self.cfg_set(ctx.guild, "autoreply_prob", prob / 100)
        await ctx.success(success)

    @commands.command()
    @commands.is_owner()
    async def block(self, ctx, user: discord.User, *, reason: str = ""):
        """Block someone from using the bot, globally.

        Only the bot owner can run this command.
        """
        user_id = user.id
        basic = self.bot.get_cog("Basic")
        try:
            await ctx.raw_send_dm(
                user_id,
                "**You have been blocked from using José.**\n"
                f"reason: `{reason}`\n"
                "If you want to appeal this block, "
                f"drop by the support guild: {basic.support_inv}",
            )
        except discord.Forbidden:
            await ctx.send("Failed to DM user")

        await ctx.success(await self.block_one(user_id, "user_id", reason))

    @commands.command()
    @commands.is_owner()
    async def unblock(self, ctx, user: discord.User):
        """Unblock someone from using the bot, globally.

        Only the bot owner can run this command.
        """
        await ctx.success(await self.unblock_one(user.id, "user_id"))

    @commands.command()
    @commands.is_owner()
    async def blockguild(self, ctx, guild_id: int, *, reason: str = ""):
        """Block an entire guild from using José.

        This makes José send a message about the block with the invite to
        the support guild, then leaving the guild.
        """
        basic = self.bot.get_cog("Basic")
        botcoll = self.bot.get_cog("BotCollection")

        guild = self.bot.get_guild(guild_id)
        if not guild:
            raise self.SayException("Guild not found")

        try:
            await botcoll.fallback(
                guild,
                "**This guild has been blocked"
                " from using José.**\n"
                f"reason: `{reason}`\n"
                "If you want to appeal this block, "
                "drop by the support guild: "
                f"{basic.support_inv}",
            )
        except discord.Forbidden:
            await ctx.send("Failed to message guild")

        await ctx.success(await self.block_one(guild_id, "guild_id", reason))
        await guild.leave()

    @commands.command()
    @commands.is_owner()
    async def unblockguild(self, ctx, guild_id: int):
        """Unblock a guild from using José."""
        await ctx.success(await self.unblock_one(guild_id, "guild_id"))

    @commands.command()
    async def blockreason(self, ctx, anything_id: int):
        """Get a reason for a block if it exists"""
        blockdata = await self.db.fetchrow(
            """
        SELECT user_id, guild_id, reason
        FROM blocks
        WHERE user_id = $1 OR guild_id = $1
        """,
            anything_id,
        )

        if blockdata is None:
            return await ctx.send("Block not found")

        actor_type = "user" if blockdata["user_id"] is not None else "guild"

        embd = discord.Embed(
            title=f"{actor_type.upper()} blocked", color=discord.Color.red()
        )

        embd.description = f'<@{anything_id}> - `{blockdata["reason"]}`'

        await ctx.send(embed=embd)

    @commands.command()
    @commands.guild_only()
    async def prefix(self, ctx, prefix: str = None):
        """Sets a guild prefix. Returns the prefix if no args are passed."""
        if not prefix:
            prefix = await self.cfg_get(ctx.guild, "prefix")
            return await ctx.send(f"The prefix is `{prefix}`")

        if not _is_moderator(ctx):
            return await ctx.send("Unauthorized to set prefix.")

        if not (1 < len(prefix) < 20):
            return await ctx.send("Prefixes must be 1-20 characters long")

        await ctx.success(await self.cfg_set(ctx.guild, "prefix", prefix))

    @commands.command()
    @commands.guild_only()
    @is_moderator()
    async def notify(self, ctx, channel: discord.TextChannel = None):
        """Make a channel a notification channel.

        A notification channel will be used by josé
        to say when your server/guild is successfully
        stolen from by another guild.

        José NEEDS TO HAVE "Send Message" permissions upfront
        for this to work.
        """
        channel = channel or ctx.channel
        perms = channel.permissions_for(ctx.guild.me)
        if not perms.send_messages:
            return await ctx.send("Add `Send Messages` " "permission to josé please")

        return await ctx.success(
            await self.cfg_set(ctx.guild, "notify_channel", channel.id)
        )

    @commands.command()
    @commands.guild_only()
    @is_moderator()
    async def artoggle(self, ctx, channel: discord.TextChannel):
        """Toggle autoreply off/on in a channel.

        By default, José autoreplies in all channels, if there are
        some channels you don't want josé to autoreply on, use this command.

        Use this command again to toggle it back on.
        """
        channels = await self.config.cfg_get(ctx.guild, "autoreply_disable", [])

        chan = channel.id
        if chan in channels:
            channels.remove(chan)
        else:
            channels.append(chan)

        ok = await self.config.cfg_set(ctx.guild, "autoreply_disable", channels)
        await ctx.success(ok)

    @commands.command()
    @commands.guild_only()
    async def arlist(self, ctx):
        """Show the channels that josé can or can not autoreply to."""
        embed = discord.Embed(title="Autoreply is disabled in")
        channels = await self.config.cfg_get(ctx.guild, "autoreply_disable", [])

        if channels:
            embed.description = " ".join(f"<#{channel}>" for channel in channels)
        else:
            embed.description = "<no channels>"

        await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    @is_moderator()
    async def mkpingtoggle(self, ctx):
        """Toggle pings off/on in a channel's markov log.

        By default, it is turned off.

        [[TODO: complete docs]]

        Use this command again to toggle it back on.
        """
        toggle = await self.config.cfg_get(ctx.guild, "speak_ping", False)
        toggle = not toggle
        ok = await self.config.cfg_set(ctx.guild, "speak_ping", toggle)
        await ctx.success(ok)


async def setup(bot):
    await bot.add_cog(Config(bot))
