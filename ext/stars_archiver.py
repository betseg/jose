import io
import os
import json
import logging
import zipfile
import tempfile

import discord
from discord.ext import commands

from .common import Cog, SayException
from .utils import Timer

log = logging.getLogger(__name__)


async def write_json(zip_file, fpath: str, obj):
    objstr = json.dumps(obj, indent=4)
    zip_file.writestr(fpath, objstr)


def _valid(content: str):
    if not content:
        return False

    if content.startswith("http://") or content.startswith("https://"):
        return False

    return True


class StarboardArchiver(Cog):
    """Archival functions for starboard."""

    def __init__(self, bot):
        super().__init__(bot)

        self._jobs = {}

    async def _write_allowed_chans(self, zip_file, cfg):
        rows = await self.pool.fetch(
            """
        SELECT channel_id
        FROM starconfig_allow
        WHERE guild_id = $1
        """,
            cfg["guild_id"],
        )

        rows = [r[0] for r in rows]

        await write_json(zip_file, "allowed_channels.json", rows)

    async def _message(self, channel_id, message_id):
        channel = self.bot.get_channel(channel_id)
        if channel is None:
            return None

        try:
            message = await channel.fetch_message(message_id)
            return message
        except (discord.NotFound, discord.Forbidden):
            return None

    async def fetch_message(self, channel_id, message_id):
        channel = self.bot.get_channel(channel_id)
        if channel is None:
            return None

        try:
            message = await channel.fetch_message(message_id)
        except (discord.NotFound, discord.Forbidden):
            return None

        return {
            "content": message.clean_content,
            "author": str(message.author),
        }

    async def _write_main_sb_data(self, ctx, zip_file, cfg):
        stars = self.bot.get_cog("Starboard")

        rows = await self.pool.fetch(
            """
        SELECT message_id, channel_id, author_id, star_message_id
        FROM starboard
        WHERE guild_id = $1
        ORDER BY star_message_id ASC
        """,
            cfg["guild_id"],
        )

        total = len(rows)
        await ctx.send(f"{total} star messages being archived...")

        res = []

        count = 0

        for row in rows:

            if (count % 250) == 0:
                await ctx.send(f"progress: {count}/{total}")

            message_id = row["message_id"]

            res.append(
                json.dumps(
                    {
                        "message_id": message_id,
                        "channel_id": row["channel_id"],
                        "author_id": row["author_id"],
                        "star_message_id": row["star_message_id"],
                        "starrers": await stars.get_starrers(message_id),
                        "message": await self.fetch_message(
                            row["channel_id"], message_id
                        ),
                    }
                )
            )

            count += 1

        final_jsonl = "\n".join(res)
        zip_file.writestr("main.jsonl", final_jsonl)

    async def _archiver(self, cfg: dict, ctx):
        """Main starboard archiver worker."""
        fd, temporary_path = tempfile.mkstemp(prefix="starboard-archive", suffix=".zip")
        log.info("saving to %s", temporary_path)
        temporary_fd = os.fdopen(fd, "wb")

        zip_file = zipfile.ZipFile(
            temporary_fd, mode="w", compression=zipfile.ZIP_DEFLATED
        )

        await write_json(zip_file, "config.json", cfg)

        with Timer() as t_sb_allow:
            await self._write_allowed_chans(zip_file, cfg)

        with Timer() as t_sb_main:
            await self._write_main_sb_data(ctx, zip_file, cfg)

        zip_file.close()
        temporary_fd.seek(0)

        # send file to discord
        attachment = discord.File(
            temporary_path, filename=f"starboard-archive-{ctx.guild.id}.zip"
        )

        await ctx.send("uploading...")

        await ctx.send(
            "finished!\n"
            "timers:\n"
            f"\tallowed_channels.json: {t_sb_allow}\n"
            f"\tmain storage: {t_sb_main}\n",
            file=attachment,
        )

        os.close(fd)
        os.unlink(temporary_path)

    async def _do_fortune(self, cfg, ctx):
        stars = self.bot.get_cog("Starboard")

        rows = await self.pool.fetch(
            """
            SELECT message_id, channel_id, author_id, star_message_id
            FROM starboard
            WHERE guild_id = $1
            ORDER BY star_message_id ASC
            """,
            cfg["guild_id"],
        )

        total = len(rows)
        await ctx.send(f"{total} star messages being fortunized...")

        res = io.BytesIO()
        count, skipped = 0, 0

        for row in rows:
            if (count % 250) == 0:
                await ctx.send(f"progress: {count}/{total}")

            message_id = row["message_id"]
            message = await self._message(row["channel_id"], message_id)
            if message is None:
                count += 1
                skipped += 1
                continue

            content = message.clean_content
            if not _valid(content):
                count += 1
                skipped += 1
                continue

            author = message.author.name
            date = message.created_at.strftime("%b %d, %Y")
            star_count = len(await stars.get_starrers(message_id))

            full_content = "\n".join(
                [
                    f'"{content}"',
                    f"- {author}, {date} {star_count}*",
                ]
            )
            res.write(full_content.encode())
            res.write("\n%\n".encode())

            count += 1

        res.seek(0)
        attachment = discord.File(res, filename=f"starboard-fortune-{ctx.guild.id}.txt")

        await ctx.send("uploading...")
        await ctx.send(
            f"finished! {count}/{total}\n"
            f"skipped over {skipped} messages (perm errors, deleted, etc)\n",
            file=attachment,
        )

    async def _archiver_wrapper(self, cfg, ctx):
        try:
            log.info("starting archiver for guild %d", ctx.guild.id)
            await ctx.send("Archival process initiated. This may take a while.")
            async with ctx.typing():
                await self._archiver(cfg, ctx)
        except Exception as err:
            log.exception("error while archiving")
            await ctx.send(f"error while archiving: {err!r}")
        finally:
            self._jobs.pop(ctx.guild.id)

    async def _fortune_wrapper(self, cfg, ctx):
        try:
            log.info("starting fortune archiver for guild %d", ctx.guild.id)
            await ctx.send("Fortune process initiated. This may take a while.")
            async with ctx.typing():
                await self._do_fortune(cfg, ctx)
        except Exception as err:
            log.exception("error while archiving")
            await ctx.send(f"error while archiving: {err!r}")
        finally:
            self._jobs.pop(ctx.guild.id)

    @commands.command()
    @commands.is_owner()
    async def sbarchive(self, ctx):
        """Start an archival job for the guild's starboard.

        This can take a long time.
        """
        if ctx.guild.id in self._jobs:
            raise SayException("There is already an archival job for the guild")

        stars = self.bot.get_cog("Starboard")

        cfg = await stars.get_starconfig(ctx.guild.id)

        if cfg is None:
            raise SayException("No starboard configuration found")

        self._jobs[ctx.guild.id] = self.loop.create_task(
            self._archiver_wrapper(cfg, ctx)
        )

        await ctx.ok()

    @commands.command()
    @commands.is_owner()
    async def sbfortune(self, ctx):
        """Start a fortune archival job for the guild's starboard.

        This can take a long time.
        """
        if ctx.guild.id in self._jobs:
            raise SayException("There is already an archival job for the guild")

        stars = self.bot.get_cog("Starboard")
        cfg = await stars.get_starconfig(ctx.guild.id)
        if cfg is None:
            raise SayException("No starboard configuration found")

        self._jobs[ctx.guild.id] = self.loop.create_task(
            self._fortune_wrapper(cfg, ctx)
        )

        await ctx.ok()


async def setup(bot):
    await bot.add_jose_cog(StarboardArchiver)
