module.exports = {
    apps: [
        {
            name: "josecoin",
            script: "pipenv run python jcoin/josecoin.py",
            env: {"PIPENV_PIPFILE": "jcoin/Pipfile"},
            restart_delay: 2000
        },
        {
            name: "jose",
            script: "pipenv run python jose.py",
            restart_delay: 5000
        }
    ]
}
