/*
 This is serving more of a general file for jose's tables
 instead of just josecoin.
 */

CREATE TABLE IF NOT EXISTS clients (
    /* Clients connecting to the API should use a token. */
    client_id text PRIMARY KEY,
    token text NOT NULL,
    client_name text NOT NULL,
    description text DEFAULT 'no description',

    /*
    0 = only fetching
    1 = full control
     */
    auth_level int NOT NULL DEFAULT 0
);

/* member table of all discord users José sees to write JOIN queries, updated in the bot itself */
CREATE TABLE IF NOT EXISTS members (
    guild_id BIGINT NOT NULL,
    user_id BIGINT NOT NULL,
    PRIMARY KEY(guild_id, user_id)
);

CREATE TABLE IF NOT EXISTS users (
    id BIGINT PRIMARY KEY,
    name TEXT NOT NULL,
    discriminator TEXT NOT NULL,
    avatar TEXT DEFAULT NULL,
    bot BOOLEAN NOT NULL
);

/* both users and taxbanks go here */
CREATE TABLE IF NOT EXISTS accounts (
    account_id bigint PRIMARY KEY NOT NULL,
    account_type int NOT NULL,
    amount money DEFAULT 0
);

CREATE VIEW account_amount as
SELECT account_id, account_type, amount::money::numeric::float8
FROM accounts;

/* only user accounts here */
CREATE TABLE IF NOT EXISTS wallets (
    user_id bigint NOT NULL REFERENCES accounts (account_id) ON DELETE CASCADE,

    taxpaid money DEFAULT 0,
    hidecoins boolean DEFAULT false,

    /* for j!steal statistics */
    steal_uses int DEFAULT 0,
    steal_success int DEFAULT 0,

    /* secondary user wallets, more of a bank */
    ubank money DEFAULT 10
);

CREATE VIEW wallets_taxpaid as
SELECT user_id, taxpaid::numeric::float8, hidecoins, steal_uses, steal_success, ubank::numeric::float8
FROM wallets;

/* The Log of all transactions */
CREATE TABLE IF NOT EXISTS transactions (
    idx serial PRIMARY KEY,
    transferred_at timestamp without time zone default now(),

    sender bigint NOT NULL REFERENCES accounts (account_id) ON DELETE RESTRICT,
    receiver bigint NOT NULL REFERENCES accounts (account_id) ON DELETE RESTRICT,
    amount numeric NOT NULL,

    /* so we can search for description='steal', or something */
    description text DEFAULT 'transfer',
    taxreturn_used boolean DEFAULT false
);


/* Steal related stuff */
CREATE TYPE cooldown_type AS ENUM ('prison', 'points');

CREATE TABLE IF NOT EXISTS steal_points (
    user_id bigint NOT NULL REFERENCES accounts (account_id),
    points int NOT NULL DEFAULT 3,
    primary key (user_id)
);

CREATE TABLE IF NOT EXISTS steal_cooldown (
    user_id bigint NOT NULL REFERENCES accounts (account_id),
    ctype cooldown_type NOT NULL,
    finish timestamp without time zone default now(),
    primary key (user_id, ctype)
);

CREATE TABLE IF NOT EXISTS steal_grace (
    user_id bigint NOT NULL PRIMARY KEY,
    finish timestamp without time zone default now()
);

CREATE VIEW steal_state as
SELECT steal_points.user_id, points, steal_cooldown.ctype, steal_cooldown.finish
FROM steal_points
JOIN steal_cooldown
ON steal_points.user_id = steal_cooldown.user_id;

/* steal historic data */
CREATE TABLE IF NOT EXISTS steal_history (
    idx serial PRIMARY KEY,
    steal_at timestamp without time zone default now(),

    /* who did what */
    thief bigint NOT NULL REFERENCES accounts (account_id) ON DELETE RESTRICT,
    target bigint NOT NULL REFERENCES accounts (account_id) ON DELETE RESTRICT,

    /* target's wallet before the steal */
    target_before numeric NOT NULL,

    /* stole amount */
    amount numeric NOT NULL,

    /* steal success context */
    success boolean NOT NULL,
    chance float8 NOT NULL,
    res float8 NOT NULL,

    motherfucker_message_id bigint DEFAULT NULL
);

/* Tax return withdraw cooldown */
CREATE TABLE IF NOT EXISTS taxreturn_cooldown (
    user_id bigint NOT NULL PRIMARY KEY,
    finish timestamp without time zone default now()
);

/* <3 */
CREATE TABLE relationships (
    user_id bigint NOT NULL,
    rel_id bigint NOT NULL,
    PRIMARY KEY (user_id, rel_id)
);

/* </3 */
CREATE TABLE restrains (
    user1 bigint,
    user2 bigint,
    PRIMARY KEY(user1, user2)
);

-- Profile badges
CREATE TABLE badges (
    badge_id bigint PRIMARY KEY,
    name text NOT NULL,
    emoji text NOT NULL,
    description text NOT NULL,
    price float8 NOT NULL
);

CREATE TABLE badge_users (
    user_id bigint NOT NULL REFERENCES accounts (account_id) ON DELETE CASCADE,
    badge bigint REFERENCES badges (badge_id) ON DELETE CASCADE,
    PRIMARY KEY (user_id, badge)
);

-- starboard configuration
CREATE TABLE starboard_config (
    -- guild, channel id
    guild_id bigint NOT NULL PRIMARY KEY,
    enabled boolean DEFAULT true,
    starboard_id bigint,
    star_emoji text NOT NULL DEFAULT '⭐',

    -- threshold of stars until starboard happens
    star_threshold bigint NOT NULL DEFAULT 1
);

-- channels which are allowed to receive stars from
CREATE TABLE starconfig_allow (
    guild_id bigint NOT NULL REFERENCES starboard_config (guild_id) ON DELETE CASCADE,
    channel_id bigint NOT NULL,
    PRIMARY KEY (guild_id, channel_id)
);

-- main starboard storage
CREATE TABLE starboard (
    message_id bigint NOT NULL PRIMARY KEY,
    channel_id bigint NOT NULL,
    guild_id bigint NOT NULL REFERENCES starboard_config (guild_id) ON DELETE CASCADE,
    author_id bigint NOT NULL,

    star_message_id bigint
);

CREATE TABLE starboard_starrers (
    message_id bigint NOT NULL REFERENCES starboard (message_id) ON DELETE CASCADE,
    starrer_id bigint NOT NULL,
    PRIMARY KEY (message_id, starrer_id)
);

CREATE TABLE starboard_blacklist (
    message_id bigint PRIMARY KEY
);

CREATE TABLE starboard_author_blacklist (
    guild_id bigint,
    user_id bigint,
    PRIMARY KEY (guild_id, user_id)
);

CREATE TABLE starboard_starrer_blacklist (
    guild_id bigint,
    user_id bigint,
    PRIMARY KEY (guild_id, user_id)
);

-- starboard.enabled can't be used because it's more of a
-- "hidden" starconfig, rather than removing star processing.
CREATE TABLE starboard_disabled (
    guild_id bigint PRIMARY KEY
);

-- pkg watchdog
CREATE TABLE pw_channels (
    guild_id bigint PRIMARY KEY,
    channel_id bigint
);

-- which packages is the guild wanting to fetch
CREATE TABLE pw_guild_packages (
    guild_id bigint NOT NULL,
    package text NOT NULL,
    PRIMARY KEY (guild_id, package)
);

-- package directory
CREATE TABLE pw_packages (
    package text PRIMARY KEY,
    version text NOT NULL,
    last_seen TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() AT TIME ZONE 'utc')
);

-- END PKG WATCHDOG --
-- START JOSE CONFIG --
CREATE TABLE IF NOT EXISTS guild_configs (
    guild_id bigint PRIMARY KEY,
    speak_channel bigint DEFAULT NULL,
    prefix text,

    -- speak cog only
    autoreply_prob float DEFAULT 0,
    autoreply_disable jsonb DEFAULT '[]',
    speak_ping BOOLEAN DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS config_custom_speak_prefixes (
    id serial PRIMARY KEY,
    guild_id BIGINT,
    prefix TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS blocks (
    user_id bigint,
    guild_id bigint,

    reason text DEFAULT ''
);

-- END JOSE CONFIG
-- START PROFILE

CREATE TABLE IF NOT EXISTS profile_desc (
    user_id bigint PRIMARY KEY,
    description text
);

-- END PROFILE
-- START MODCFG

CREATE TABLE IF NOT EXISTS mod_config (
    guild_id bigint PRIMARY KEY,
    member_log_id bigint,
    mod_log_id bigint,
    last_action_id bigint DEFAULT 0
);

-- START LOTTERY

CREATE TABLE IF NOT EXISTS lottery_tickets (
    user_id bigint PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS lottery_cooldowns (
    user_id bigint PRIMARY KEY,
    rolls_wait bigint
);

-- END LOTTERY

-- START BBL

CREATE TABLE IF NOT EXISTS bbl_stats (
    user_id BIGINT NOT NULL,
    project TEXT NOT NULL,
    provider TEXT NOT NULL,

    server_count BIGINT NOT NULL,
    PRIMARY KEY (user_id, project, provider)
);

-- END BBL
